# 前提

以下がインストール済みであること

- oc
- htpasswd (httpd-tools)
- jq

# 実行方法

- 環境の準備

    ```
    git clone https://gitlab.com/k-srkw/container-starter-kit-preparation.git
    cd container-starter-kit-preparation
    prepare.sh <apiserver> <admin-user> <admin-pass> <handson-usernum>
    ```

- Etherpad および説明・ハンズオン資料をホストする httpd の URL 確認

    ```
    oc status
    または
    oc get route
    ```

- 説明・ハンズオン資料の更新

    資料を `text/` 配下に格納後以下を実行

    ```
    update-text.sh
    ```
