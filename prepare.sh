#!/bin/bash

if test $# != 4;then
 echo "prepare.sh <apiserver> <admin-user> <admin-pass> <handson-usernum>"
 exit
fi

APISERVER=$1
ADMINUSER=$2
ADMINPASS=$3
USERNUM=$4
HANDSON_NAMESPACE="handson"

oc login -u ${ADMINUSER} -p ${ADMINPASS} ${APISERVER}

htpasswd -c -B -b users.htpasswd user1 openshift
for i in $(seq 2 ${USERNUM}); do
    htpasswd -b users.htpasswd user$i openshift
done
oc get secret -n openshift-config htpasswd-secret -o json | jq .data.htpasswd -r | base64 -d | egrep "andrew|karla|opentlc-mgr" >> users.htpasswd
oc get secret -n openshift-config htpasswd-secret -o yaml > htpasswd-secret.yaml.bk
oc create secret generic htpasswd-secret --from-file=htpasswd=users.htpasswd --dry-run=client -o yaml -n openshift-config | oc replace -f -

sleep 60

oc new-project $HANDSON_NAMESPACE

oc process postgresql-persistent --param-file=etherpad-db-template-param.txt --labels=app=etherpad_db -n openshift | oc apply -f -
oc process -f https://raw.githubusercontent.com/wkulhanek/docker-openshift-etherpad/master/etherpad-template.yaml --param-file=etherpad-template-param.txt | oc apply -f -
oc new-app --name='httpd' httpd~./text
sleep 60
oc start-build httpd --from-dir=./text
oc expose svc httpd

oc apply -f web-terminal-operator-subs.yaml

for userid in $(seq ${USERNUM}); do
    echo user$userid
    oc login -u user${userid} -p openshift $(oc whoami --show-server)
    users+=("user${userid}")
done

oc login -u ${ADMINUSER} -p ${ADMINPASS} ${APISERVER}

oc adm groups new handson-cluster-admins "${users[@]}"
oc adm policy add-cluster-role-to-group cluster-admin handson-cluster-admins --rolebinding-name=handson-cluster-admins
